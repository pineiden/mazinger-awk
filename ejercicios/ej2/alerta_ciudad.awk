#!/usr/bin/awk -f

function celsius2fahrenheit(celsius){
    fahrenheit = celsius*(9/5)+32;
    return fahrenheit
}

function alerta(ciudad, fahrenheit){
    city = "";
    if (fahrenheit >= 68){
        city = toupper(ciudad);
    }   
    else {
        city = ciudad
    }
    return city
}
#
# Se inicia programa
#
BEGIN{
    C_PAIS=1;
    C_CIUDAD=2;
    C_TEMP=3;
    FS=",";
    OFS=";";
    print "país","ciudad","temperatura °[F]"; 
}
(NR>1){
    FAHR=celsius2fahrenheit($C_TEMP);
    CIUDAD = alerta($C_CIUDAD, FAHR);
    print $C_PAIS,CIUDAD,FAHR
}