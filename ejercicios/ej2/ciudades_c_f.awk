#!/usr/bin/awk

#
# 
# SE DEFINEN LAS FUNCIONES
# 
#
function celsius2farenheit(celsius)
{
    farenheit = celsius*(9/5)+32;
    return farenheit
}

function alerta(ciudad, farenheit)
{
    city="";
    if (farenheit>=68)
    {
        city = toupper(ciudad)}
    else{
        city = ciudad}
    return city
}
#
#
# SE PROGRAMA LA LECTURA DEL ARCHIVO
#
#
BEGIN{
vPAIS=1;
vCIUDAD=2;
vTEMP=3;
FS=",";
OFS=";";
print "país","ciudad","temperatura °[F]"
}
(NR>1){
FAR=celsius2farenheit($vTEMP);
CIUDAD=alerta($vCIUDAD,FAR);
print $vPAIS,CIUDAD,FAR
}


